package com.ruyuan.seckill.config;

import com.ruyuan.seckill.utils.StringUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Redis配置
 *
 * 修改文件位置,增加RedissonClinet的配置
 */
@Configuration
public class SeckillRedisConfig {

    @Bean
    public RedissonClient redissonClient(RedisConnectionConfig config) {
        Config rconfig = new Config();
        String host = config.getHost();
        int port = config.getPort();
        SingleServerConfig singleServerConfig = rconfig.useSingleServer().setAddress("redis://" + host + ":" + port);
        if (!StringUtil.isEmpty(config.getPassword())) {
            singleServerConfig.setPassword(config.getPassword());
        }
        return Redisson.create(rconfig);
    }


}
