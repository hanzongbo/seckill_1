package com.ruyuan.seckill.service.cartbuilder;


import com.ruyuan.seckill.domain.vo.CartVO;

import java.util.List;

/**
 * 数据正确性校验器
 */
public interface CheckDataRebderer {
    /**
     * 数据正确性校验
     *
     * @param cartList 购物车数据
     */
    void checkData(List<CartVO> cartList);
}
