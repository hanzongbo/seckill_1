package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.MemberAddress;

/**
 * 会员地址业务层
 */
public interface MemberAddressManager {


    /**
     * 获取会员地址
     *
     * @param id 会员地址主键
     * @return MemberAddress  会员地址
     */
    MemberAddress getModel(Integer id);

    /**
     * 获取会员默认地址
     *
     * @param memberId 会员id
     * @return 会员默认地址
     */
    MemberAddress getDefaultAddress(Integer memberId);




}