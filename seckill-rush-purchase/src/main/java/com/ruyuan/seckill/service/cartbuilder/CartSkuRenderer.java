package com.ruyuan.seckill.service.cartbuilder;


import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.enums.CartType;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.service.cartbuilder.impl.CartSkuFilter;

import java.util.List;

/**
 * 购物车sku渲染器，负责生产一个全新的cartList
 */
public interface CartSkuRenderer {

    /**
     * 渲染sku数据
     */
    void renderSku(List<CartVO> cartList, CartType cartType, CheckedWay way);
    void renderSku(List<CartVO> cartList, CartType cartType, Buyer buyer, CheckedWay way);

    /**
     * 渲染sku数据
     * 带过滤器式的
     */
    void renderSku(List<CartVO> cartList, CartSkuFilter cartFilter, CartType cartType, CheckedWay way);
}
