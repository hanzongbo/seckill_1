package com.ruyuan.seckill.service.cartbuilder.impl;


import com.ruyuan.seckill.cache.Cache;
import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.enums.CartType;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import com.ruyuan.seckill.domain.vo.CartSkuOriginVo;
import com.ruyuan.seckill.domain.vo.CartSkuVO;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.domain.vo.ShopVO;
import com.ruyuan.seckill.service.CartOriginDataManager;
import com.ruyuan.seckill.service.cartbuilder.CartSkuRenderer;
import com.ruyuan.seckill.utils.CartUtil;
import com.ruyuan.seckill.utils.CurrencyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 购物车sku渲染实现
 */
@Service
@Primary
public class CartSkuRendererImpl implements CartSkuRenderer {


    @Autowired
    private CartOriginDataManager cartOriginDataManager;

    @Autowired
    private Cache cache;

    @Override
    public void renderSku(List<CartVO> cartList, CartType cartType, CheckedWay way) {
        List<CartSkuOriginVo> originList = cartOriginDataManager.read(way);

        //用原始数据渲染购物车
        for (CartSkuOriginVo originVo : originList) {

            innerRunder(originVo, cartList, cartType);
        }
    }

    @Override
    public void renderSku(List<CartVO> cartList, CartType cartType, Buyer buyer, CheckedWay way) {
        // 获取当前用户要购买的商品
        List<CartSkuOriginVo> originList = cartOriginDataManager.read(way, buyer);

        //用原始数据渲染购物车
        for (CartSkuOriginVo originVo : originList) {

            innerRunder(originVo, cartList, cartType);
        }
    }

    @Override
    public void renderSku(List<CartVO> cartList, CartSkuFilter cartSkuFilter, CartType cartType, CheckedWay way) {
        //获取原始数据
        List<CartSkuOriginVo> originList = cartOriginDataManager.read(way);

        //用原始数据渲染购物车
        for (CartSkuOriginVo originVo : originList) {

            //转换为购物车skuvo
            CartSkuVO skuVO = toSkuVo(originVo);

            //如果过滤成功才继续
            if (!cartSkuFilter.accept(skuVO)) {
                continue;
            }

            innerRunder(originVo, cartList, cartType);
        }
    }


    /**
     * 内部化用的渲染方法
     *
     * @param originVo
     * @param cartList
     * @param cartType
     */
    private void innerRunder(CartSkuOriginVo originVo, List<CartVO> cartList, CartType cartType) {
        // 获取卖家id
        Integer sellerId = originVo.getSellerId();
        // 获取卖家姓名
        String sellerName = originVo.getSellerName();

        // 获取店铺信息
        ShopVO shopVO = (ShopVO) cache.get("seller::" + sellerId);
        if (shopVO != null) {
            sellerName = shopVO.getShopName();
        }

        //转换为购物车skuvo
        CartSkuVO skuVO = toSkuVo(originVo);
        CartVO cartVO = CartUtil.findCart(sellerId, cartList);
        if (cartVO == null) {
            cartVO = new CartVO(sellerId, sellerName, cartType);
            cartList.add(cartVO);
        }

        //压入到当前店铺的sku列表中
        cartVO.getSkuList().add(skuVO);
    }
    /**
     * 内部化用的渲染方法
     *
     * @param originVo
     * @param cartList
     * @param cartType
     */
    private void innerRunder(CartSkuOriginVo originVo, List<CartVO> cartList, Buyer buyer,CartType cartType) {
        Integer sellerId = originVo.getSellerId();
        String sellerName = originVo.getSellerName();

        ShopVO shopVO = (ShopVO) cache.get("seller::" + sellerId);
        if (shopVO != null) {
            sellerName = shopVO.getShopName();
        }

        //转换为购物车skuvo
        CartSkuVO skuVO = toSkuVo(originVo);
        CartVO cartVO = CartUtil.findCart(sellerId, cartList);
        if (cartVO == null) {
            cartVO = new CartVO(sellerId, sellerName, cartType);
            cartList.add(cartVO);
        }

        //压入到当前店铺的sku列表中
        cartVO.getSkuList().add(skuVO);
    }

    /**
     * 将一个 CartSkuOriginVo转为  CartSkuVO
     *
     * @param originVo
     * @return
     */
    @SuppressWarnings("Duplicates")
    private CartSkuVO toSkuVo(CartSkuOriginVo originVo) {

        // 生成一个购物项
        CartSkuVO skuVO = new CartSkuVO();
        skuVO.setSellerId(originVo.getSellerId());
        skuVO.setSellerName(originVo.getSellerName());
        skuVO.setGoodsId(originVo.getGoodsId());
        skuVO.setSkuId(originVo.getSkuId());
        skuVO.setCatId(originVo.getCategoryId());
        skuVO.setGoodsImage(originVo.getThumbnail());
        skuVO.setName(originVo.getGoodsName());
        skuVO.setSkuSn(originVo.getSn());
        skuVO.setPurchasePrice(originVo.getPrice());
        skuVO.setOriginalPrice(originVo.getPrice());
        skuVO.setSpecList(originVo.getSpecList());
        skuVO.setIsFreeFreight(originVo.getGoodsTransfeeCharge());
        skuVO.setGoodsWeight(originVo.getWeight());
        // 运费模板id
        skuVO.setTemplateId(originVo.getTemplateId());
        // 可销售库存
        skuVO.setEnableQuantity(originVo.getEnableQuantity());
        skuVO.setLastModify(originVo.getLastModify());
        skuVO.setNum(originVo.getNum());
        skuVO.setChecked(originVo.getChecked());
        skuVO.setGoodsType(originVo.getGoodsType());

        //计算小计
        double subTotal = CurrencyUtil.mul(skuVO.getNum(), skuVO.getOriginalPrice());
        skuVO.setSubtotal(subTotal);

        return skuVO;
    }

}
