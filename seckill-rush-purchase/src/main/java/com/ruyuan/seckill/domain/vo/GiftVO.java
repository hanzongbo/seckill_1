package com.ruyuan.seckill.domain.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 赠品VO
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class GiftVO implements Serializable {

    @ApiModelProperty(value = "赠品ID")
    private Integer giftId;

    @ApiModelProperty(value = "商家ID")
    private Integer sellerId;

    @ApiModelProperty(value = "赠品名称")
    private String giftName;

    @ApiModelProperty(value = "数量")
    private Integer giftNum;


    public Integer getGiftId() {
        return giftId;
    }

    public void setGiftId(Integer giftId) {
        this.giftId = giftId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public Integer getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(Integer giftNum) {
        this.giftNum = giftNum;
    }

    @Override
    public String toString() {
        return "GiftVO{" +
                "giftId=" + giftId +
                ", sellerId=" + sellerId +
                ", giftName='" + giftName + '\'' +
                ", giftNum=" + giftNum +
                '}';
    }

}
