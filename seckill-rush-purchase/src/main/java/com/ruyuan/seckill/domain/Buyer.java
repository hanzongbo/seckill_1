package com.ruyuan.seckill.domain;

import com.ruyuan.seckill.domain.enums.Role;

import java.io.Serializable;

public class Buyer extends User implements Serializable {


    /**
     * 定义买家的角色
     */
    public Buyer() {
        this.add(Role.BUYER.name());
    }

}
